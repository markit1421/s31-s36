//[SECTION] Dependencies and Modules
const exp = require("express");
const controller = require('../controllers/users');
const auth = require('../auth');

//Destructure verify from auth
const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
const route = exp.Router();

//[SECTION] [POST] Routes
route.post('/register', (req, res) => {
    let data = req.body;
    controller.registerUser(data).then(outcome => {
        res.send(outcome);
    });
});

route.post('/login', controller.loginUser);

route.post('/enroll', verify, controller.enroll);

//[SECTION] [GET] Routes
route.get('/all', (req, res) => {
    controller.getAllUsers().then(outcome => {
        res.send(outcome);
    });
});

route.get('/userDetails', verify, controller.getUserDetails);

route.get('/getEnrollments', verify, controller.getEnrollments)

//[SECTION] [PUT] Routes
//[SECTION] [DEL] Routes


//[SECTION] Export Route System
module.exports = route;