//[SECTION] Dependencies and Modules
const User = require('../models/User');
const Course = require('../models/Course');
const auth = require('../auth');
const bcrypt = require('bcrypt');
const dotenv = require('dotenv');
dotenv.config();
const salt = parseInt(process.env.SALT);

//[SECTION] Functionality [CREATE]
module.exports.registerUser = (info) => {
    let firstN = info.firstname;
    let lastN = info.lastname;
    let eml = info.email;
    let pw = info.password;
    let gndr = info.gender;
    let mobNum = info.mobileNo;


    let newUser = new User({
        firstName: firstN,
        lastName: lastN,
        email: eml,
        password: bcrypt.hashSync(pw, salt),
        gender: gndr,
        mobileNo: mobNum
    })
    return newUser.save().then((savedUser, error) => {
        if (error) {
            return 'Failed to register new user';
        } else {
            return savedUser;
        }
    });
};

module.exports.loginUser = (req, res) => {

    User.findOne({
            email: req.body.email
        })
        .then(result => {
            if (result === null) {
                return res.send({message:'User Not Found'});
            } else {
                // return 'Email Found'; 
                const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);
               
                if (isPasswordCorrect) {
                    return res.send({
                        accessToken: auth.createAccessToken(result)
                    });
                } else {
                    return res.send(false);
                }
            }
        })
        .catch(err => res.send(err));
};

module.exports.getUserDetails = (req,res)=>{
    User.findById(req.user.id)
    .then(result => res.send(result))
    .catch(err => res.send(err))
}

module.exports.enroll = async (req,res) =>{
    // console.log(req.user.id)
    // console.log(req.body.courseId)

    if(req.user.isAdmin){
        return res.send('Action Forbidden');
    }

    let isUserUpdated = await User.findById(req.user.id).then (user =>{
        let newEnrollment={
            courseId: req.body.courseId
        }
        user.enrollments.push(newEnrollment);

        return user.save().then(user => true).catch(err => err.message);
    });

     if(isUserUpdated !== true) {
      return res.send({message: isUserUpdated})
    }

    let isCourseUpdated = await Course.findById(req.body.courseId).then (course =>{
        let enrollee={
            userId: req.user.id
        }

        course.enrollees.push(enrollee);

        return course.save().then(course => true).catch(err => err.message);
    });

    if(isCourseUpdated !== true) {
      return res.send({message: isCourseUpdated})
    }


    if(isUserUpdated && isCourseUpdated){
        return res.send({ message: "Enrolled Successfully."})
    }



}


//[SECTION] Functionality [RETRIEVE]
module.exports.getAllUsers = () => {

    return User.find({}).then(resultUsers => {
        return resultUsers;
    });
}

module.exports.getEnrollments = (req,res) =>{
    User.findById(req.user.id).then(result=> res.send(result.enrollments))
    .catch(err => res.send(err))
}

//[SECTION] Functionality [UPDATE]
//[SECTION] Functionality [DELETE]