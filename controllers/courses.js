//[SECTION] Dependencies and Modules
const Course = require('../models/Course');

//[SECTION] Functionality [CREATE]
module.exports.createCourse = (info) => {
    let cName = info.name;
    let cDesc = info.description;
    let cCost = info.price;
    let newCourse = new Course({
        name: cName,
        description: cDesc,
        price: cCost
    })
    return newCourse.save().then((savedCourse, error) => {
        if (error) {
            return false;
        } else {
            return savedCourse;
        }
    });
};

//[SECTION] Functionality [RETRIEVE]
module.exports.getAllCourse = () => {

        return Course.find({}).sort({name: 1}).then(resultCourses => {
            return resultCourses;
        });
    }
    //Retrieve a single course
module.exports.getCourse = (id) => {
        return Course.findById(id).then(resultOneCourse => {
            return resultOneCourse;
        });
    }
    //Retrieve a active courses
module.exports.getAllActiveCourse = () => {

    return Course.find({
        isActive: true
    }).then(resultActiveCourses => {
        return resultActiveCourses;
    });
}

//[SECTION] Functionality [UPDATE]
module.exports.updateCourse = (id, details) => {
    let cName = details.name;
    let cDesc = details.description;
    let cCost = details.price;

    let updatedCourse = {
          name: cName,
          description: cDesc,
          price: cCost
    }
    
    return Course.findByIdAndUpdate(id, updatedCourse).then((courseUpdated, err) => {
        if (err) {
            return false;
        } else {
            return courseUpdated;
        }
    });
}

module.exports.deactivateCourse = (id) => {
    let updates={
        isActive: false
    }
    return Course.findByIdAndUpdate(id, updates).then((courseArchived, err) => {
        if (err) {
            return 'Error deactivating course.';
        } else {
            return `Course with id number ${id} has been deactivated.`;
        }
    });
}

module.exports.reactivateCourse = (id) => {
    let updateActive={
        isActive: true
    }
    return Course.findByIdAndUpdate(id, updateActive).then((courseActivate, err) => {
        if (err) {
            return 'Error activating course.';
        } else {
            return `Course with id number ${id} has been activated.`;
        }
    });
}

//[SECTION] Functionality [DELETE]
module.exports.deleteCourse = (id) => {

    return Course.findByIdAndRemove(id).then((removedCourse, err) => {
        if (err) {
            return 'No Course was removed.';
        } else {
            return 'Course succesfully deleted.';
        }
    });
}