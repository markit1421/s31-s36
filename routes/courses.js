//[SECTION] Dependencies and Modules
const exp = require("express");
const controller = require('../controllers/courses');

const auth = require('../auth');

//Destructure verify from auth
const {verify, verifyAdmin} = auth;

//[SECTION] Routing Component
const route = exp.Router();

//[SECTION] [POST] Routes
route.post('/create',verify,verifyAdmin, (req, res) => {
    let data = req.body;
    controller.createCourse(data).then(outcome => {
        res.send(outcome);
    });
});


//[SECTION] [GET] Routes
route.get('/all', (req, res) => {
    controller.getAllCourse().then(outcome => {
        res.send(outcome);
    });
});

route.get('/:id', (req, res) => {
    let courseID = req.params.id;
    controller.getCourse(courseID).then(outcome => {
        res.send(outcome);
    });
});

route.get('/', (req, res) => {
    controller.getAllActiveCourse().then(outcome => {
        res.send(outcome);
    });
});

//[SECTION] [PUT] Routes
route.put('/:id', (req, res) => {
    let data = req.body;
    let courseID = req.params.id;
    let cName = data.name;
    let cDesc = data.description;
    let cCost = data.price;

    if (cName.trim() == '' || cDesc.trim() == '' || cCost == '') {
        res.send({message:'All fields are required.'});
    } else {
        controller.updateCourse(courseID, data).then(outcome => {
            res.send(true);
        });
    }

});

route.put('/:id/archive', (req, res) => {
    let courseID = req.params.id;
    controller.deactivateCourse(courseID).then(outcome => {
            res.send(outcome);
    });
});

route.put('/:id/reactivate', (req, res) => {
    let courseID = req.params.id;
    controller.reactivateCourse(courseID).then(outcome => {
            res.send(outcome);
    });
});

//[SECTION] [DEL] Routes
route.delete('/:id', (req, res) => {
    let courseID = req.params.id;
    controller.deleteCourse(courseID).then(outcome => {
        res.send(outcome);
    });
});


//[SECTION] Export Route System
module.exports = route;