// Packages and dependencies
const mongoose = require('mongoose');
const express = require('express');
const dotenv = require('dotenv');
  const cors = require("cors");
const courseRoutes = require('./routes/courses');
const userRoutes = require('./routes/users');  

// Server setup
const app = express();
app.use(express.json());
dotenv.config();
const con = process.env.CONNECTION_STRING;
const port = process.env.PORT;
app.use(cors())

//[SECTION] Application Routes
app.use('/courses',courseRoutes); 
app.use('/users',userRoutes); 

// Database connection
mongoose.connect(con);

let dbStatus = mongoose.connection;

dbStatus.on('open', () => console.log('Database connection successful!'));

//[SECTION] Gateway Response
app.get('/',(req,res)=>{
	res.send('Welcome to Mark-IT-Done Solutions!');
})

app.listen(port, () => console.log(`Server is running on ${port}`));



